package us.craigmiller160.gradle.plugins.githooks

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermission
import org.gradle.api.Project

const val PRE_COMMIT =
    """
#!/bin/sh

echo "Running spotless check..."

files=${'$'}(git diff --name-only --staged)

./gradlew spotlessApply
status=${'$'}?

if [ ${'$'}status -ne 0 ]; then
  exit 1
fi

for file in ${'$'}files; do
  git add ${'$'}file
done

exit 0
"""

const val GUARD_FILE = "craig-hooks-v2"
const val PRE_COMMIT_FILE = "pre-commit"
val GIT_DIR_RELATIVE: Path = Paths.get(".git")
val GIT_HOOKS_DIR_RELATIVE: Path = GIT_DIR_RELATIVE.resolve(Paths.get("hooks"))

private fun writePreCommitFile(projectDir: File) {
  val gitHooksDir = projectDir.toPath().resolve(GIT_HOOKS_DIR_RELATIVE)
  if (!Files.exists(gitHooksDir)) {
    Files.createDirectory(gitHooksDir)
  }

  val preCommitPath = gitHooksDir.resolve(Paths.get(PRE_COMMIT_FILE))
  Files.write(preCommitPath, PRE_COMMIT.toByteArray())
  Files.setPosixFilePermissions(
      preCommitPath,
      setOf(
          PosixFilePermission.OWNER_READ,
          PosixFilePermission.OWNER_WRITE,
          PosixFilePermission.OWNER_EXECUTE,
          PosixFilePermission.GROUP_READ,
          PosixFilePermission.GROUP_EXECUTE,
          PosixFilePermission.OTHERS_READ,
          PosixFilePermission.OTHERS_EXECUTE))
}

fun Project.createSpotlessGitHook() {
  pluginManager.withPlugin("com.diffplug.spotless") {
    val guardFile = projectDir.toPath().resolve(GIT_HOOKS_DIR_RELATIVE).resolve(GUARD_FILE)
    if (Files.exists(guardFile)) {
      logger.debug("Default git hooks already exist, skipping creating")
    } else {
      logger.info("Installing default Git hooks")
      writePreCommitFile(projectDir)
      Files.createFile(guardFile)
    }
  }
}
