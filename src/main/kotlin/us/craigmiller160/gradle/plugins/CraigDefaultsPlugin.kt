package us.craigmiller160.gradle.plugins

import org.gradle.api.Plugin
import org.gradle.api.Project
import us.craigmiller160.gradle.plugins.building.fixJarForSpring
import us.craigmiller160.gradle.plugins.githooks.createSpotlessGitHook
import us.craigmiller160.gradle.plugins.publishing.setupPublishing

class CraigDefaultsPlugin : Plugin<Project> {
  override fun apply(project: Project) {
    project.createSpotlessGitHook()
    project.setupPublishing()
    project.fixJarForSpring()
  }
}
