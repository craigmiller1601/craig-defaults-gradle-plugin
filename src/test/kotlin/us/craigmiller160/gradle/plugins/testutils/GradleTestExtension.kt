package us.craigmiller160.gradle.plugins.testutils

import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Properties
import kotlin.io.path.createDirectories
import kotlin.io.path.createFile
import kotlin.io.path.inputStream
import kotlin.io.path.writeText
import org.apache.commons.io.FileUtils
import org.gradle.testkit.runner.GradleRunner
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ExtensionContext.Namespace
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver
import us.craigmiller160.gradle.plugins.githooks.GIT_DIR_RELATIVE
import us.craigmiller160.gradle.plugins.githooks.GIT_HOOKS_DIR_RELATIVE

class GradleTestExtension :
    BeforeEachCallback, AfterEachCallback, ParameterResolver, BeforeAllCallback, AfterAllCallback {
  companion object {
    const val PROJECT_NAME = "gradle-plugin-test-project"

    private const val WORKING_DIR_KEY = "WORKING_DIR"
    private const val BUILD_FILE_KEY = "BUILD_FILE"
    private const val SETTINGS_FILE_KEY = "SETTINGS_FILE"
    private const val GRADLE_RUNNER_KEY = "GRADLE_RUNNER"
    private const val PLUGIN_VERSION_KEY = "PLUGIN_VERSION"
  }

  private val workingDirRoot =
      Paths.get(System.getProperty("user.dir"), "src", "test", "resources", "testWorkingDir")

  override fun beforeEach(context: ExtensionContext) {
    workingDirRoot.resolve(GIT_DIR_RELATIVE).apply { createDirectories() }
    val testKitDir = workingDirRoot.resolve("testKit").apply { createDirectories() }

    val gradleRunner =
        GradleRunner.create()
            .withPluginClasspath()
            .withProjectDir(workingDirRoot.toFile())
            .withTestKitDir(testKitDir.toFile())

    val buildFile = workingDirRoot.resolve("build.gradle.kts").apply { createFile() }
    val settingsFile = workingDirRoot.resolve("settings.gradle.kts").apply { createFile() }
    settingsFile.writeText("""rootProject.name = "$PROJECT_NAME" """)

    context.getStore(Namespace.create(GradleTestExtension::class.java)).let { store ->
      store.put(WORKING_DIR_KEY, workingDirRoot)
      store.put(BUILD_FILE_KEY, buildFile)
      store.put(SETTINGS_FILE_KEY, settingsFile)
      store.put(GRADLE_RUNNER_KEY, gradleRunner)
    }
  }

  override fun afterEach(context: ExtensionContext) {
    context.getStore(Namespace.create(GradleTestExtension::class.java)).let { store ->
      Files.delete(store.get(BUILD_FILE_KEY) as Path)
      Files.delete(store.get(SETTINGS_FILE_KEY) as Path)

      val gitHooksDir = workingDirRoot.resolve(GIT_HOOKS_DIR_RELATIVE)
      FileUtils.deleteDirectory(gitHooksDir.toFile())
      store.remove(WORKING_DIR_KEY)
      store.remove(BUILD_FILE_KEY)
      store.remove(SETTINGS_FILE_KEY)
      store.remove(GRADLE_RUNNER_KEY)
    }
  }

  override fun beforeAll(context: ExtensionContext) {
    val gradleProperties = Paths.get(System.getProperty("user.dir"), "gradle.properties")
    val properties = Properties().apply { load(gradleProperties.inputStream()) }

    context.getStore(Namespace.create(GradleTestExtension::class.java)).let { store ->
      store.put(PLUGIN_VERSION_KEY, properties.getProperty("projectVersion"))
    }
  }

  override fun afterAll(context: ExtensionContext) {
    context.getStore(Namespace.create(GradleTestExtension::class.java)).let { store ->
      store.remove(PLUGIN_VERSION_KEY)
    }
  }

  override fun supportsParameter(
      parameterContext: ParameterContext,
      extensionContext: ExtensionContext
  ): Boolean = parameterContext.parameter.type == GradleTestContext::class.java

  override fun resolveParameter(
      parameterContext: ParameterContext,
      extensionContext: ExtensionContext
  ): Any {
    val store = extensionContext.getStore(Namespace.create(GradleTestExtension::class.java))
    return when (parameterContext.parameter.type) {
      GradleTestContext::class.java -> {
        val runner = store.get(GRADLE_RUNNER_KEY) as GradleRunner
        val buildFile = store.get(BUILD_FILE_KEY) as Path
        GradleTestContext(
            runner = store.get(GRADLE_RUNNER_KEY) as GradleRunner,
            buildFile = store.get(BUILD_FILE_KEY) as Path,
            pluginVersion = store.get(PLUGIN_VERSION_KEY) as String,
            workingDir = store.get(WORKING_DIR_KEY) as Path)
      }
      else ->
          throw IllegalArgumentException(
              "Invalid parameter type: ${parameterContext.parameter.type}")
    }
  }
}
