package us.craigmiller160.gradle.plugins.githooks

import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.paths.shouldExist
import io.kotest.matchers.paths.shouldNotExist
import io.kotest.matchers.shouldBe
import java.nio.file.Paths
import java.nio.file.attribute.PosixFilePermission
import kotlin.io.path.createDirectories
import kotlin.io.path.createFile
import kotlin.io.path.getPosixFilePermissions
import kotlin.io.path.readText
import org.gradle.testkit.runner.TaskOutcome
import org.gradle.testkit.runner.internal.DefaultBuildTask
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import us.craigmiller160.gradle.plugins.testutils.GradleTestContext
import us.craigmiller160.gradle.plugins.testutils.GradleTestExtension
import us.craigmiller160.gradle.plugins.testutils.shouldHaveExecuted

@ExtendWith(GradleTestExtension::class)
class SetupSpotlessGitHookTest {
  @Test
  fun `does nothing when spotless plugin is not present`(context: GradleTestContext) {
    val script =
        """
      plugins {
        id("us.craigmiller160.gradle.defaults") version "${context.pluginVersion}"
        kotlin("jvm") version "1.8.20"
      }
    """
            .trimIndent()
    context.writeBuildScript(script)

    val result = context.runner.withArguments("init", "--stacktrace").build()
    result.tasks.shouldHaveExecuted(DefaultBuildTask(":init", TaskOutcome.SKIPPED))

    val preCommitPath = context.workingDir.resolve(Paths.get(".git", "hooks", "pre-commit"))
    preCommitPath.shouldNotExist()
  }

  @Test
  fun `writes pre-commit git hook when spotless plugin is present, creating hooks directory`(
      context: GradleTestContext
  ) {
    val script =
        """
          import com.diffplug.gradle.spotless.SpotlessExtension
          
          plugins {
            id("us.craigmiller160.gradle.defaults") version "${context.pluginVersion}"
            kotlin("jvm") version "1.8.20"
            id("com.diffplug.spotless") version "6.17.0"
          }
          
          configure<SpotlessExtension> {
            kotlin {
              ktfmt()
            }
          }
      """
            .trimIndent()
    context.writeBuildScript(script)

    val result = context.runner.withArguments("init", "--stacktrace").build()
    result.tasks.shouldHaveExecuted(DefaultBuildTask(":init", TaskOutcome.SKIPPED))

    val preCommitPath = context.workingDir.resolve(GIT_HOOKS_DIR_RELATIVE).resolve(PRE_COMMIT_FILE)
    preCommitPath.shouldExist()
    preCommitPath.readText().shouldBe(PRE_COMMIT)

    val guardFilePath = context.workingDir.resolve(GIT_HOOKS_DIR_RELATIVE).resolve(GUARD_FILE)
    guardFilePath.shouldExist()

    preCommitPath
        .getPosixFilePermissions()
        .shouldContainAll(
            PosixFilePermission.OWNER_READ,
            PosixFilePermission.OWNER_WRITE,
            PosixFilePermission.OWNER_EXECUTE,
            PosixFilePermission.GROUP_READ,
            PosixFilePermission.GROUP_EXECUTE,
            PosixFilePermission.OTHERS_READ,
            PosixFilePermission.OTHERS_EXECUTE)
  }

  @Test
  fun `does nothing when spotless plugin is present but guard file is also present`(
      context: GradleTestContext
  ) {
    val script =
        """
          import com.diffplug.gradle.spotless.SpotlessExtension
          
          plugins {
            id("us.craigmiller160.gradle.defaults") version "${context.pluginVersion}"
            kotlin("jvm") version "1.8.20"
            id("com.diffplug.spotless") version "6.17.0"
          }
          
          configure<SpotlessExtension> {
            kotlin {
              ktfmt()
            }
          }
      """
            .trimIndent()
    context.writeBuildScript(script)

    val hooksDir = context.workingDir.resolve(GIT_HOOKS_DIR_RELATIVE).apply { createDirectories() }
    hooksDir.resolve(Paths.get(GUARD_FILE)).apply { createFile() }

    val result = context.runner.withArguments("init", "--stacktrace").build()
    result.tasks.shouldHaveExecuted(DefaultBuildTask(":init", TaskOutcome.SKIPPED))

    val preCommitPath = context.workingDir.resolve(Paths.get(".git", "hooks", "pre-commit"))
    preCommitPath.shouldNotExist()
  }

  @Test
  fun `writes pre-commit git hook when spotless plugin is present and hooks directory exists`(
      context: GradleTestContext
  ) {
    context.workingDir.resolve(GIT_HOOKS_DIR_RELATIVE).apply { createDirectories() }
    val script =
        """
          import com.diffplug.gradle.spotless.SpotlessExtension
          
          plugins {
            id("us.craigmiller160.gradle.defaults") version "${context.pluginVersion}"
            kotlin("jvm") version "1.8.20"
            id("com.diffplug.spotless") version "6.17.0"
          }
          
          configure<SpotlessExtension> {
            kotlin {
              ktfmt()
            }
          }
      """
            .trimIndent()
    context.writeBuildScript(script)

    val result = context.runner.withArguments("init", "--stacktrace").build()
    result.tasks.shouldHaveExecuted(DefaultBuildTask(":init", TaskOutcome.SKIPPED))

    val preCommitPath = context.workingDir.resolve(GIT_HOOKS_DIR_RELATIVE).resolve(PRE_COMMIT_FILE)
    preCommitPath.shouldExist()
    preCommitPath.readText().shouldBe(PRE_COMMIT)

    val guardFilePath = context.workingDir.resolve(GIT_HOOKS_DIR_RELATIVE).resolve(GUARD_FILE)
    guardFilePath.shouldExist()

    preCommitPath
        .getPosixFilePermissions()
        .shouldContainAll(
            PosixFilePermission.OWNER_READ,
            PosixFilePermission.OWNER_WRITE,
            PosixFilePermission.OWNER_EXECUTE,
            PosixFilePermission.GROUP_READ,
            PosixFilePermission.GROUP_EXECUTE,
            PosixFilePermission.OTHERS_READ,
            PosixFilePermission.OTHERS_EXECUTE)
  }
}
